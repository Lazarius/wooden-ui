# wooden-ui
A nice old medieval fantasy UI for FVTT, based on fantasy-UI work.

## Setup
To install this module, go to the World Configuration and Setup, Addon Modules, Install Module.
Then you may copy this url https://gitlab.com/Lazarius/wooden-ui/-/raw/master/module.json

## Screenshot
![alt text](https://nsa40.casimages.com/img/2021/01/04/210104103801381730.jpg)

## Acknowledgement
[iotech](https://foundryvtt.com/community/iotech) for his good work on [fantasy-ui theme](https://foundryvtt.com/packages/fantasy-ui/).

## Artworks
All pictures come from [Free 3D RPG Icons](https://line.17qq.com/articles/wsppkhsy.html) (RPG Icons polycount) on 17qq.com